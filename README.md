# Swing Your Partner #
A WordPress theme for SteveClason.com, a portfolio site and blog for a Web Developer (me).
To maximize speed, a lot of the site features, like social media links, navigation menus, and credits, have been hard-coded into the templates. The idea was to keep everything static except for the content and the main content delivery system (what WordPress call "The Loop") so that there would be as little code running as possible while the content was still stored in a portable format so it could be re-skinned again.

If you don't know how to develop WiordPress themes then you need those widgets, but it's easy for an experienced developer to revise a theme file in the case of a change.

I recently started using MarkDown for the content (pages and posts), which increases the portability a little, but the main goal was to be able to reuse the page-and-post content with a complete redesign.

## Features: ##
* fast. very fast
* spare, minimalist
* **no** JavaScript libraries
* Portfolio CPT
* Skills taxonomy
* Reviews CPT

## Things To Do: ##
* Pages for portfolio and review
* Delete blogroll
