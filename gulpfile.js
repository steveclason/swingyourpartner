
var gulp = require('gulp');
var sass = require( 'gulp-sass' );
var sourcemaps = require('gulp-sourcemaps');
// var rename = require( 'gulp-rename' );
// var minify = require( 'gulp-minify-css' );
var autoprefixer = require( 'gulp-autoprefixer' );
// var util = require( 'gulp-util' );
var log = require( 'fancy-log' );
//require( 'stylelint' )(),

gulp.task( 'sass', function () {
  'use strict';
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./src/swingyourpartner/sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './dist/swingyourpartner' ))
    .pipe( gulp.dest( 'D:/Projects/steveclason-local/wp-content/themes/swingyourpartner' ));
  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './tilghman' ));
});

gulp.task( 'editor-sass', function () {
  'use strict';
  log( 'Generate CSS files ' + (new Date()).toString());
  return gulp
    .src('./src/swingyourpartner/sass/editor-style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe( autoprefixer( 'last 4 version' ))
    .pipe( sourcemaps.write('.'))
    .pipe( gulp.dest( './dist/swingyourpartner' ))
    .pipe( gulp.dest( 'D:/Projects/steveclason-local/wp-content/themes/swingyourpartner' ));
  // .pipe( rename({ suffix: '.min' }))
  // .pipe(minify())
  // .pipe(gulp.dest( './tilghman' ));
});

// gulp.task('sass:watch', function () {
//   gulp.watch('./inn97win/sass/**/*.scss', ['sass']);
// });

gulp.task( 'copy', function() {
  'use strict';
  log( 'Deploy files to dist ' + (new Date()).toString());
  return gulp
    .src( './src/swingyourpartner/**/*', { base: './src/swingyourpartner' } )
    .pipe( gulp.dest( './dist/swingyourpartner' ))
    // .src( '.dist/themes/swingyourpartner/**/*', { base: '.dist/themes/swingyourpartner' } )
    .pipe( gulp.dest( 'D:/Projects/steveclason-local/wp-content/themes/swingyourpartner' ));
});

gulp.task( 'deploy', gulp.series( 'sass', 'editor-sass', 'copy' ));
