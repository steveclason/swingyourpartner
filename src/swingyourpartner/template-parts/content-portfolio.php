<?php
/*
 * Display format for Portfolio post-types.
 *
 * Selected if post-type is "Portfolio, takes data from custom fields to populate page headings,
 * Schema.org mark-up and some meta data.
 *
 * @author Steve Clason <steve@steveclason.com>
 * @package steveclason-blog
 *
 */

// TODO: add custom fields: url, skills, challenges, date completed, designer, project management?
// Add new taxonomy for skills: HTML, CSS, JavaScript, jQuery, WordPress, Joomla, PHP, MySQL,
// Backbone.js, Angular.js

// TODO Use ItemList to mark this up. http://schema.org/ItemList
?>

<article <?php post_class() ?> id="post-<?php the_ID(); ?>" itemtype="http://schema.org/ItemList" itemscope="" >
	<header>
		<h3 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'swingyourpartner'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
		<h4><?php the_terms( $post->ID, 'skills', 'Tech used: ', ', ', ' ' );	?></h4>
		<div class="entry-meta">

		<?php

		// Add fields with author and updated schema.
		swingyourpartner_posted_on();
		?>
		</div>
	</header>

	<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search.  ?>
		<?php
			the_post_thumbnail( 'thumbnail' );
			the_content();
			 ?>
	<?php else : ?>
		<div itemprop="item">

		<?php

		the_post_thumbnail( 'single-post-thumbnail' );
		the_content(__('(more...)', 'swingyourpartner' ));
		// portfolio_url, portfolio_name, portfolio_date, portfolio_designer, portfolio_manager
		echo "<ul class='portfolio-meta'>";
//		var_dump (get_post_custom($postid )['portfolio-url'][0]);
		global $post;
		$postid = $post -> ID;

		if( get_post_meta( $postid, 'portfolio-name', true ) !== '' ) {
			echo '<li class="portfolio-name"><i class="far fa-building"></i>Client: ' . get_post_meta( $postid, 'portfolio-name', true ) . '</li>';
		}

		if( get_post_meta( $postid, 'portfolio-url', true ) !== '' ) {
			echo '<li class="portfolio-url"><i class="far fa-address-card"></i>Site URL: <a href="' . get_post_meta( $postid, 'portfolio-url', true ) . '">' .
					get_post_meta( $postid, 'portfolio-url', true ) . '</a></li>';
		}

		if( get_post_meta( $postid, 'portfolio-date', true ) !== '' ) {
			echo '<li class="portfolio-date"><i class="far fa-calendar-alt"></i>Project Date: ' .
					get_post_meta( $postid, 'portfolio-date', true ) . '</li>';
		}

		if( get_post_meta( $postid, 'portfolio-designer', true ) !== '' ) {
			echo '<li class="portfolio-designer"><i class="far fa-pencil-alt"></i>Site Designer: ' .
					get_post_meta( $postid, 'portfolio-designer', true ) . '</li>';
		}

		if( get_post_meta( $postid, 'portfolio-manager', true ) !== '' ) {
			echo '<li class="portfolio-manager"><i class="far fa-bullhorn"></i>Project Manager: ' .
					get_post_meta( $postid, 'portfolio-manager', true ) . '</li>';
		}

		echo "</ul>";
		?>
		</div>

		<?php wp_link_pages(array('before' => '<nav>' . __( 'Pages:', 'swingyourpartner' ), 'after' => '</nav>')); ?>
	<?php endif; ?>

	<footer>

	</footer>
</article>
