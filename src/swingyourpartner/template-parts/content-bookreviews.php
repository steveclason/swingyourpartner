<?php
/*
 * Display format for "review" category blog posts.
 *
 * Selected if post category = "Review", takes data from custom fields to populate page headings
 * and some meta data.
 *
 * @author Steve Clason <steve@steveclason.com>
 * @package steveclason-blog
 */
?>
<article <?php post_class() ?> id="post-<?php the_ID(); ?>" itemprop="blogPost" itemtype="http://schema.org/Review" itemscope="" >
	<header>
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf(esc_attr__('Permalink to %s', 'swingyourpartner'), the_title_attribute('echo=0')); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<div class="entry-meta"><?php review_posted_on(); ?></div>
	</header>

	<?php if (is_archive() || is_search()) : // Only display excerpts for archives and search.  ?>
		<?php the_excerpt(); ?>
	<?php else : ?>
		<div itemprop="reviewBody">
		<?php the_content(__('(more...)', 'swingyourpartner')); ?>
		</div>
	<?php // Only show the Book details if there is at least a Title and an Author.
	$postid = get_the_ID();
	$time = get_post_meta($postid, 'PubDate', true); // Should be in format 'June 2013'
	$pubDate = DateTime::createFromFormat('M Y', $time);

	if( get_post_meta($postid, 'Title', true) !== '' && get_post_meta($postid, 'Author', true ) !== '' ): ?>
	<div class="bookDetails">
		<div itemtype="http://schema.org/Book" itemscope="" itemprop="itemReviewed">
			<span class="book-review__name" itemprop="name"><?php echo get_post_meta($postid, 'Title', true); ?></span>
			 by
			<span class="book-review__author"itemprop="author"><?php echo get_post_meta($postid, 'Author', true); ?></span><br/>
			<?php if( get_post_meta($postid, 'Publisher', true ) !== '' ) : ?>
			<span itemprop="publisher" class="book-review__publisher"><?php echo get_post_meta($postid, 'Publisher', true); ?></span>
			<?php
			endif;

			if( $pubDate && $pubDate !== '' ): ?><br/>
				<meta content="<?php echo $pubDate->format('Y-m-d'); ?>" itemprop="datePublished" />
				<?php echo $pubDate->format('F Y');
			endif; ?>

			<?php if( get_post_meta($postid, 'NumPages', true) !== '' ): ?>
				<br/>
				<span itemprop="numberOfPages" class="book-review__num-pages"><?php echo get_post_meta($postid, 'NumPages', true); ?></span> pages
			<?php endif; ?>
		</div><!-- End of Book scope. -->

		<?php if( get_post_meta($postid, 'Rating', true) !== '' ): ?>
			<div itemprop="reviewRating" itemtype="http://schema.org/Rating" itemscope="" class="rating">
				<meta content="1" itemprop="worstRating" />
				<?php
				$myRating = get_post_meta($postid, 'Rating', true);
				for( $i = 0; $i < 5; $i++ ) {
					if( $i < $myRating ) {
						echo '<i class="fa fa-star"></i>';
					} else {
						echo '<i class="fa fa-star-o"></i>';
					}
				}
				?>
				(<span itemprop="ratingValue"><?php echo $myRating;  ?></span> / <span itemprop="bestRating">5</span> stars)
			</div><!-- End of .rating. -->
		<?php endif; // End of get_post_meta. ?>
	</div><!-- End of bookDetails. -->
		<?php endif; // End of get_post_meta. ?>


		<?php wp_link_pages(array('before' => '<nav>' . __('Pages:', 'swingyourpartner'), 'after' => '</nav>')); ?>
	<?php endif; ?>

	<footer>

		<?php if (count(get_the_category())) : ?>
			<span class="footer-item">
				<?php printf(__('Posted in %2$s', 'swingyourpartner'), 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list(', ')); ?>
			</span>
		<?php endif; ?>

		<?php
		$tags_list = get_the_tag_list('', ', ');
		if ($tags_list):
			?>
			<span class="footer-item">
				<?php printf(__('Tagged %2$s', 'swingyourpartner'), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list); ?>
			</span>
		<?php endif; ?>
		<span class="footer-item">
			<?php comments_popup_link(__('Leave a comment', 'swingyourpartner'), __('1 Comment', 'swingyourpartner'), __('% Comments', 'swingyourpartner')); ?>
		</span>
		<span class="footer-item">
			<?php edit_post_link(__('Edit', 'swingyourpartner'), '', ''); ?>
		</span>
	</footer>

	<?php comments_template('', true); ?>

</article>
