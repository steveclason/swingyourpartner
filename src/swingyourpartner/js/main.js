/* Requires that fontfaceobserver.js is already loaded. */
var sansSerifFont = new FontFaceObserver( 'Noto Sans' );
var serifFont = new FontFaceObserver( 'Noto Serif' );
var iconFont = new FontFaceObserver( 'FontAwesome' );

Promise.all([sansSerifFont.load(), serifFont.load(), iconFont.load()]).then(function () {
	document.documentElement.className += ' fonts-loaded';
});
