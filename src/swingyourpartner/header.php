<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">SteveClason.com</a></h1>

			<h2 class="site-description">Web Development &amp; Stuff</h2>

		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<div class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i class="far fa-bars"></i></div>
			<?php // FIXME: Replace dynamic code with static list. ?>
			<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			<div class="menu-main-menu-container">
				<ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
					<li class="menu-item">
						<a href="/boulder-web-developer/"><i class="far fa-hand-spock"></i>About</a>
					</li>
					<li class="menu-item">
						<a href="/portfolio"><i class="far fa-briefcase"></i>Portfolio</a>
					</li>
					<li class="menu-item">
						<?php get_search_form(); ?>
					</li>
					<li>
						<i class="far fa-archive"></i>
						<select name="archive-dropdown"
										onchange="document.location.href=this.options[this.selectedIndex].value;">
						  <option value=""><?php esc_attr( _e( 'Archives', 'textdomain' ) ); ?></option>
						  <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
						</select>
					</li>
					<li>
						<i class="far fa-list"></i>
						<select name="event-dropdown"
										onchange="document.location.href=this.options[this.selectedIndex].value;">
					    <option value=""><?php echo esc_attr_e( 'Categories', 'textdomain' ); ?></option>
					    <?php
					    $categories = get_categories();
					    foreach ( $categories as $category ) {
					        printf( '<option value="%1$s">%2$s (%3$s)</option>',
					            esc_attr( '/category/archives/' . $category->category_nicename ),
					            esc_html( $category->cat_name ),
					            esc_html( $category->category_count )
					        );
					    }
					    ?>
						</select>
					</li>
					<li>
						<i class="far fa-hourglass"></i>Recently...
						<ul>
						<?php
							$args = array( 'numberposts' => '5' );
							$recent_posts = wp_get_recent_posts( $args );
							foreach( $recent_posts as $recent ){
								echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
							}
							wp_reset_query();
						?>
						</ul>
					</li>
				</ul>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
