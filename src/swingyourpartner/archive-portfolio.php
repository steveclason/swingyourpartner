<?php
/**
 * The template for displaying Portfolio Archive pages.
 *
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main role="main" class="site-main">
		<h2>Some Stuff I&#039;ve Worked On</h2>
		<div itemscope itemtype="http://schema.org/ItemList">

		<?php
		query_posts( array(
			'orderby' => 'menu_order',
			'order' => 'DESC',
			'post_type' => 'portfolio',
			'posts_per_page' => -1 ) );

		while ( have_posts() ) {
			the_post();
			get_template_part('template-parts/content', 'portfolio');
		}  // End the loop.
		?>

		</div><!-- End of ItemList itemscope. -->
	</main>
</div>


<?php
get_sidebar();
get_footer();
