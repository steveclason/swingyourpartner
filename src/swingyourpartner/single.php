<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			$postType = get_post_type();
			if( get_post_type() === "bookreviews" ) {
				get_template_part( 'template-parts/content', 'bookreviews' );
				$nav_title = 'Navigate Book Reviews';
			} elseif( get_post_type() === "portfolio" ) {
				get_template_part( 'template-parts/content', 'portfolio' );
				$nav_title = 'Navigate Portfolio';
			} else {
				get_template_part( 'template-parts/content', 'single' );
				$nav_title = '';
			}

			swingyourpartner_post_nav( $nav_title ); // In inc/template_tags.php, defaults to "Post Navigation".
			// FIXME: Delete this.
			// the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
