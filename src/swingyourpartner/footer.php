<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swingyourpartner
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<div class="footer__social">
				<a href="https://plus.google.com/113119086998910334041/profile" rel="author"><i class="fab fa-google-plus"></i></a>
				<a href="https://codepen.io/SteveClason/#"><i class="fab fa-codepen" aria-hidden="true"></i></a>
				<a href="http://stackoverflow.com/users/6334817/steve-clason?tab=profile"><i class="fab fa-stack-overflow" aria-hidden="true"></i></a>
				<a href="https://github.com/steveclason"><i class="fab fa-github"></i></a>
				<a href="https://bitbucket.org/steveclason/"><i class="fab fa-bitbucket"></i></a>
			</div>

			<div class="footer__license">
				<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><i class="fab fa-creative-commons"></i></a>
				<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">SteveClason.com</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.steveclason.com" property="cc:attributionName" rel="cc:attributionURL">Steve Clason</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
			</div>

			<div class="footer__credits">
				<p>
					Powered by <a href="https://wordpress.org/download/beta/">WordPress Version <?php echo get_bloginfo( 'version' ); ?></a>. <span>Theme is <a href="https://bitbucket.org/steveclason/swingyourpartner/overview">SwingYourPartner</a> by <a href="mailto:info@steveclason.com">Steve Clason</a> (duh).</span>
				</p>
			</div>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "LocalBusiness",
	"url": "http://www.steveclason.com",
	"telephone": "1-303-818-8590",
	"email": "info@steveclason.com",
	"address": {
		"@type": "PostalAddress",
		"streetAddress": "3305 Eastman Ave.",
		"postalCode": "80305",
		"addressLocality": "Boulder",
		"addressRegion": "Colorado",
		"addressCountry": "USA"
	},
	"name": "SteveClason.com",
	"description": "Steve Clason is a freelance Web Developer in Boulder, Colorado, expert in front-end web work, especially WordPress themes and Joomla templates but also custom applications with HTML, CSS, JavaScript (jQuery, React, etc.), PHP, and MySQL.",
	"geo": {
		"@type": "GeoCoordinates",
		"latitude": "39.98",
		"longitude": "-105.25"
	},
	"foundingDate": "2001",
	"isicV4": "6201",
	"naics": "541511",
	"sameAs" : [
		"https://codepen.io/SteveClason/#",
		"https://github.com/steveclason",
		"http://stackoverflow.com/users/6334817/steve-clason?tab=profile",
		"https://plus.google.com/u/0/+SteveClason",
		"https://bitbucket.org/steveclason/"
	]
}
</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-47262527-1', 'auto');
	ga('send', 'pageview');
</script>

<?php wp_footer(); ?>
</body>
</html>
