<form action="/" method="get">
  <a id="search-button"><i class="fa fa-search"></i></a>
	<input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search..." />
</form>
