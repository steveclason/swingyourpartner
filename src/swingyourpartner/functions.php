<?php
/**
 * _s functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! function_exists( 'swingyourpartner_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function swingyourpartner_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _s, use a find and replace
	 * to change 'swingyourpartner' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'swingyourpartner', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'swingyourpartner_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'swingyourpartner_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function swingyourpartner_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'swingyourpartner_content_width', 640 );
}
add_action( 'after_setup_theme', 'swingyourpartner_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function swingyourpartner_scripts() {
	// wp_dequeue_stylesheet( 'swingyourpartner-style-css' );
	// wp_enqueue_style( 'swingyourpartner-style', get_stylesheet_uri() );
	// wp_enqueue_style( 'swingyourpartner-noto', "https://fonts.googleapis.com/css?family=Noto+Sans|Noto+Serif" );
	// wp_enqueue_style( 'swingyourpartner-fa', 'https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css' );

	wp_enqueue_script( 'swingyourpartner-navigation', get_template_directory_uri() . '/js/navigation.js', '', '', true );
	wp_enqueue_script( 'swingyourpartner-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', '', '', true );
  wp_enqueue_script( 'swingyoutpartner-fontawesome', get_template_directory_uri() . '/vendor/fontawesome/js/all.js', '', '', true );

// TODO: Do I need to enqueue the base stylesheet?
  wp_enqueue_style( 'swingyourpartner', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'swingyourpartner-fonts', 'https://fonts.googleapis.com/css?family=Noto+Sans:400,700|Noto+Serif:400,700');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'swingyourpartner_scripts' );

/*
	Defer loading of as many scripts as you can.
	script_loader_tag is new as of WP 4.1
 */
 // TODO: Why does this break Gutenberg?
function swingyourpartner_defer_scripts( $tag, $handle ) {

	// Put handle for scripts that cannot be deferred in this array.
	$no_defer = array( 'jquery-core', 'jquery-migrate' );
	if ( in_array( $handle, $no_defer )) {
		return $tag;
	} else {
		return str_replace( ' src', ' defer="defer" src', $tag );
	}
}
// add_filter( 'script_loader_tag', 'swingyourpartner_defer_scripts', 10, 2 );

// Allow shortcodes in sidebar widgets.
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );

// Add shortcode to use RICG ResponsiveImages.
// $id is the image id, size is a text string set by the theme.
function tevkori_img( $atts ) {
	$a = shortcode_atts( array(
      'id' => '1',
      'size' => 'large',
  ), $atts );

  return tevkori_get_srcset_string( $a[ 'id' ], $a[ 'size' ] );
}
add_shortcode( 'rimg', 'tevkori_img' );

/*
	Modify the 'read more' link.
 */
function modify_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '">(read more <i class="fa fa-long-arrow-right"></i>)</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );
/**
 * Add image sizes appropriate to theme layout: name, width, height, crop.
 * https://developer.wordpress.org/reference/functions/add_image_size/
 *
 * Here in conjunction with RICG-Responsive-Images:
 * https://github.com/ResponsiveImagesCG/wp-tevko-responsive-images
 *
 */
//add_image_size('small', 370, 275, true);
//add_image_size('medium', 770, 400, true);
//add_image_size('large', 170, 128, true);
//add_image_size( 'excerpt-thumb', 198, 145, true );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';

/**
 * Load custom post types and taxonomies..
 */
 // TODO: enable this for production.
require get_template_directory() . '/inc/custom-post-types.php';
