<?php
/**
 * Portfolio Custom Post Type
 */

// TODO: add custom fields: name, url, skills, challenges, date completed, designer, project management?
// Add new taxonomy for skills: HTML, CSS, JavaScript, jQuery, WordPress, Joomla, PHP, MySQL,
// Backbone.js, Angular.js

// TODO: Change icon for post types.
function theme_register_post_type_portfolio() {
	register_post_type( 'portfolio',
		array(
			'labels' => array(
				'name' => __( 'Portfolio' ),
				'singular_name' => __( 'Portfolio Item' ),
				'new_item' => __('New Portfolio Item'),
				'add_new_item' => __('Add New Portfolio Item')
			),
		'menu_name' => "Portfolio",
		'description' => 'Portfolio item with thumbnail and skills listing.',
		'public' => true,
		'has_archive' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'thumbnail',
			'editor',
			'custom-fields',
			'revisions',
			'page-attributes',
			'post-formats',
			'wpcom-markdown'
			)
		)
	);
}

add_action( 'init', 'theme_register_post_type_portfolio' );


function portfolio_url(){
	global $post;
  $custom = get_post_custom( $post -> ID );
  if( $custom && $custom !== '' ) {
    $portfolio_url = $custom["portfolio-url"][0];
  } else {
	  $portfolio_url = '';
  }

	?>
		<label>URL:</label>
		<input name="portfolio-url" value="<?php echo $portfolio_url; ?>" />
	<?php
}

function portfolio_name(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if( $custom && $custom !== '' ) {
    $portfolio_name = $custom["portfolio-name"][0];
  } else {
	  $portfolio_name = '';
  }
	?>
		<label>Name (Client):</label>
		<input name="portfolio-name" value="<?php echo $portfolio_name; ?>" />
	<?php
}

function portfolio_date(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if( $custom && $custom !== '' ) {
    $portfolio_date = $custom["portfolio-date"][0];
  } else {
	  $portfolio_date = '';
  }
	?>
		<label>Date:</label>
		<input name="portfolio-date" value="<?php echo $portfolio_date; ?>" />
	<?php
}

function portfolio_designer(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if( $custom && $custom !== '' ) {
    $portfolio_designer = $custom["portfolio-designer"][0];
  } else {
	  $portfolio_designer = '';
  }
	?>
		<label>Designer:</label>
		<input name="portfolio-designer" value="<?php echo $portfolio_designer; ?>" />
	<?php
}

function portfolio_manager(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if( $custom && $custom !== '' ) {
    $portfolio_manager = $custom["portfolio-manager"][0];
  } else {
	  $portfolio_manager = '';
  }
	?>
		<label>Project Manager:</label>
		<input name="portfolio-manager" value="<?php echo $portfolio_manager; ?>" />
	<?php
}

function portfolio_init(){
	//
	add_meta_box( "portfolio_url-meta", "Portfolio Item URL", "portfolio_url", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_name-meta", "Portfolio Item Name", "portfolio_name", "portfolio", "normal", "low" );
	add_meta_box( "portfolio-date-meta", "Portfolio Item Date", "portfolio_date", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_designer-meta", "Portfolio Item Designer", "portfolio_designer", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_manager-meta", "Portfolio Item Project Manager", "portfolio_manager", "portfolio", "normal", "low" );
}

add_action("admin_init", "portfolio_init");

function theme_register_skills_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Skills', 'taxonomy general name' ),
		'singular_name'              => _x( 'Skill', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Skills' ),
		'popular_items'              => __( 'Popular Skills' ),
		'all_items'                  => __( 'All Skills' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Skill' ),
		'update_item'                => __( 'Update Skill' ),
		'add_new_item'               => __( 'Add New SKill' ),
		'new_item_name'              => __( 'New Skill' ),
		'separate_items_with_commas' => __( 'Separate skills with commas' ),
		'add_or_remove_items'        => __( 'Add or remove skills' ),
		'choose_from_most_used'      => __( 'Choose from the most used skills' ),
		'not_found'                  => __( 'No skills found.' ),
		'menu_name'                  => __( 'Skills' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'skill' ),
	);

	register_taxonomy( 'skills', 'portfolio', $args );
}

add_action( 'init', 'theme_register_skills_taxonomy', 0 );




// Save Portfolio Items Duh.
function save_portfolio(){
	global $post;
	if( $post && $post->post_type === 'portfolio' ) {
		update_post_meta($post->ID, "portfolio-url", $_POST["portfolio-url"]);
		update_post_meta($post->ID, "portfolio-name", $_POST["portfolio-name"]);
		update_post_meta($post->ID, "portfolio-date", $_POST["portfolio-date"]);
		update_post_meta($post->ID, "portfolio-designer", $_POST["portfolio-designer"]);
		update_post_meta($post->ID, "portfolio-manager", $_POST["portfolio-manager"]);
	}
}

add_action('save_post', 'save_portfolio');

/* End Portfolio Custom Post Type */

/**
 * Book Reviews Custom Post Type
 */
// Register a Custom Post Type for Book Reviews.
function swingyourpartner_register_post_type_bookreview() {
	register_post_type( 'bookreviews',
		array(
			'labels' => array(
				'name' => __( 'Book Reviews' ),
				'singular_name' => __( 'Book Review' ),
				'new_item' => __('New Review'),
				'add_new_item' => __('Add New Review')
			),
		'menu_name' => "Book Review",
		'description' => 'Book review with star ratings.',
		'public' => true,
		'has_archive' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'editor',
			'author',
			'custom-fields',
			'revisions',
			'page-attributes',
			'post-formats',
			'wpcom-markdown'
			)
		)
	);
}

// Include the bookreview post type on the main blog.
// Show posts of 'post', 'page' and 'movie' post types on home page
add_action( 'pre_get_posts', 'add_bookreviews_to_query' );

function add_bookreviews_to_query( $query ) {
	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'bookreviews' ) );
	return $query;
}

// Register the custom post type with the init hook.
add_action( 'init', 'swingyourpartner_register_post_type_bookreview' );

// Add meta box for the required custom field types.


function bookreview_init(){
	// Author, Title, Publisher, PubDate, NumPages, Rating
	add_meta_box( "book_author-meta", "Book Author", "book_author", "bookreviews", "side", "low" );
	add_meta_box( "book_title-meta", "Book Title", "book_title", "bookreviews", "side", "low" );
	add_meta_box( "book_publisher-meta", "Book Publisher", "book_publisher", "bookreviews", "side", "low" );
	add_meta_box( "book_pubDate-meta", "Book Publication Date", "book_pubDate", "bookreviews", "side", "low" );
	add_meta_box( "book_numPages-meta", "Book Number of Pages", "book_numPages", "bookreviews", "side", "low" );
	add_meta_box( "book_rating-meta", "Book Rating (1-5)", "book_rating", "bookreviews", "side", "low" );

}

add_action("admin_init", "bookreview_init");

function book_author(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$author = $custom["Author"][0];
	?>
		<label>Author:</label>
		<input name="Author" value="<?php echo $author; ?>" />
	<?php
}

function book_title(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$title = $custom["Title"][0];
	?>
		<label>Title:</label>
		<input name="Title" value="<?php echo $title; ?>" />
	<?php
}

function book_publisher(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$publisher = $custom["Publisher"][0];
	?>
		<label>Publisher:</label>
		<input name="Publisher" value="<?php echo $publisher; ?>" />
	<?php
}

function book_pubDate(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$pubDate = $custom["PubDate"][0];
	?>
		<label>Publication Date:</label>
		<input name="PubDate" value="<?php echo $pubDate; ?>" />
	<?php
}

function book_numPages(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$numPages = $custom["NumPages"][0];
	?>
		<label>Number of Pages:</label>
		<input name="NumPages" value="<?php echo $numPages; ?>" />
	<?php
}

function book_rating(){
	global $post;
	$custom = get_post_custom( $post -> ID );
	$rating = $custom["Rating"][0];
	?>
		<label>Rating:</label>
		<input name="Rating" value="<?php echo $rating; ?>" />
	<?php
}

add_action('save_post', 'save_review_details');

// Author, Title, Publisher, PubDate, NumPages, Rating
function save_review_details(){
	global $post;
	if( $post && $post->post_type === 'bookreviews' ) {
		update_post_meta($post->ID, "Author", $_POST["Author"]);
		update_post_meta($post->ID, "Title", $_POST["Title"]);
		update_post_meta($post->ID, "Publisher", $_POST["Publisher"]);
		update_post_meta($post->ID, "PubDate", $_POST["PubDate"]);
		update_post_meta($post->ID, "NumPages", $_POST["NumPages"]);
		update_post_meta($post->ID, "Rating", $_POST["Rating"]);
	}
}

/**
 * Special posted_on display for reviews. Renders the modified date if that differs from the
 * posted_on date, and contains microdata mark-up.
 */
if (!function_exists( 'review_posted_on' )) {
	function review_posted_on() {
		printf( __('Reviewed by <span class="vcard author"><span class="fn" itemprop="author">%3$s</span></span> on %2$s', 'swingyourpartner' ),
		'meta-prep meta-prep-author',
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time datetime="%3$s" class="updated" itemprop="dateCreated">%4$s</time></a>',
				get_permalink(),
				esc_attr( get_the_time() ),
				get_the_date( 'Y-m-d' ),
				get_the_date()
			),
			sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
				get_author_posts_url( get_the_author_meta( 'ID' ) ),
				sprintf( esc_attr__( 'View all posts by %s', 'swingyourpartner' ), get_the_author() ),
				get_the_author()
			)
		);
		// If post has been modified, display the modified date.
	  if( get_the_date() !== get_the_modified_date() || get_the_time() !== get_the_modified_time() ) {
		printf( __( '<br/>Updated: <span itemprop="dateModified">%2$s</span>', 'swingyourpartner' ),
			'meta-prep meta-prep-author',
			sprintf( '<time datetime="%3$s">%4$s</time>',
			get_permalink(),
			esc_attr( get_the_modified_time() ),
			get_the_modified_date('Y-m-d'),
			get_the_modified_date() )
		);
	  }
	}
}
/**
 * End Book Reviews Custom Post Type
 */
