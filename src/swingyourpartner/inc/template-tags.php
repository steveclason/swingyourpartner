<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package swingyourpartner
 */

if ( ! function_exists( 'swingyourpartner_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function swingyourpartner_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'swingyourpartner' ) );
		if ( $categories_list && swingyourpartner_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'swingyourpartner' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'swingyourpartner' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'swingyourpartner' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'swingyourpartner' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'swingyourpartner' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function swingyourpartner_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'swingyourpartner_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'swingyourpartner_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so swingyourpartner_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so swingyourpartner_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in swingyourpartner_categorized_blog.
 */
function swingyourpartner_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'swingyourpartner_categories' );
}
add_action( 'edit_category', 'swingyourpartner_category_transient_flusher' );
add_action( 'save_post',     'swingyourpartner_category_transient_flusher' );

if ( ! function_exists( 'swingyourpartner_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 */
function swingyourpartner_paging_nav() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'steveclason-flat' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'steveclason-flat' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'steveclason-flat' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'swingyourpartner_post_nav' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 */
function swingyourpartner_post_nav( $nav_title = 'Post Navigation') {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php echo $nav_title; ?></h1>
		<div class="nav-links">
			<?php
				previous_post_link( '<div class="nav-previous">%link</div>', _x( '<i class="fa fa-arrow-left"></i> %title', 'Previous post link', 'steveclason-flat' ) );
				next_post_link(     '<div class="nav-next">%link</div>',     _x( '%title <i class="fa fa-arrow-right"></i>', 'Next post link',     'steveclason-flat' ) );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if (!function_exists( 'swingyourpartner_posted_on' )) {
	/**
	 * Prints HTML with meta information for the current post's date/time and author. Adds the
	 * modified date if that differs from posted date. Contains microdata mark-up.
	 */
	function swingyourpartner_posted_on() {
	  printf(
			__( 'Posted on %1$s by %2$s', 'swingyourpartner' ),

			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time datetime="%3$s" class="updated" itemprop="dateCreated">%4$s</time></a>',
			  get_permalink(),
			  esc_attr( get_the_time() ),
			  get_the_date('Y-m-d'),
			  get_the_date()
			),
			sprintf( '<a href="%1$s" title="%2$s"><span class="vcard author"><span class="fn" itemprop="author">%3$s</span></span></a>',
			  get_author_posts_url( get_the_author_meta( 'ID' ) ),
			  sprintf( esc_attr__( 'View all posts by %s', 'swingyourpartner' ), get_the_author() ),
			  get_the_author()
			)
	  );
	  // If post has been modified, display the modified date.
	  if( get_the_date() !== get_the_modified_date() || get_the_time() !== get_the_modified_time() ) {
		printf( __( '<br/>Updated: <span itemprop="dateModified">%1$s</span>', 'swingyourpartner' ),

			sprintf( '<time datetime="%3$s">%4$s</time>',
			get_permalink(),
			esc_attr( get_the_modified_time() ),
			get_the_modified_date('Y-m-d'),
			get_the_modified_date() )
		);
	  }
	}
}
